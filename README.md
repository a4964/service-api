# Service API

Servizio che espone API per la manipolazione e salvataggio su database oltre che ricezione e invio dati tramite RabbitMQ.

## Build e push immagine Docker

Prima di buildare e pushare l'immagine su registry Docker esterno è necessario buildare e pubblicare l'applicativo tramite Visual Studio o dotnet CLI. Attualmente viene utilizzato la location di pubblicazione di default nella cartella `./ServiceApi/ServiceApi/bin/Release/net5.0/publish/`.

Successivamente è possibile effettuare il build e push dell'immagine tramite questi comandi:

```
docker build -t andreasosi/microservices-project:service-api .

docker push andreasosi/microservices-project:service-api

```

## Swagger API

La documentazione per le API è visualizzabile al seguente link:

http://localhost:5000/swagger
