﻿using System;

namespace ServiceApi.Models
{
    public class Message
    {
        public int MessageId { get; set; }
        public string MessageContent { get; set; }
        public int? ReferenceId { get; set; }
        public DateTime? ReferenceDateTime { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime? UpdateDateTime { get; set; }
    }
}
