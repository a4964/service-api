﻿using Microsoft.EntityFrameworkCore;
using ServiceApi.Models;

namespace ServiceApi.DBContext
{
        public class DatabaseContext : DbContext
        {
            public DbSet<Message> Messages { get; set; }

            public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
            {
                Database.EnsureCreated();
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                // Map entities to tables  
                modelBuilder.Entity<Message>().ToTable("Messages");

                // Configure Primary Keys  
                modelBuilder.Entity<Message>().HasKey(u => u.MessageId).HasName("PK_Messages");

                // Configure columns  
                modelBuilder.Entity<Message>().Property(u => u.MessageId).HasColumnType("int").UseMySqlIdentityColumn().IsRequired();
                modelBuilder.Entity<Message>().Property(u => u.MessageContent).HasColumnType("nvarchar(50)").IsRequired();
                modelBuilder.Entity<Message>().Property(u => u.ReferenceId).HasColumnType("int").IsRequired(false);
                modelBuilder.Entity<Message>().Property(u => u.ReferenceDateTime).HasColumnType("datetime").IsRequired(false);
                modelBuilder.Entity<Message>().Property(u => u.CreateDateTime).HasColumnType("datetime").IsRequired();
                modelBuilder.Entity<Message>().Property(u => u.UpdateDateTime).HasColumnType("datetime").IsRequired(false);
            }
        }
}
