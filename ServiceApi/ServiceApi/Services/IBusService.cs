﻿using MessageModel;
using ServiceApi.DBContext;
using System.Threading.Tasks;

namespace ServiceApi.Service
{
    public interface IBusService
    {
        /// <summary>
        /// Save the message into the database
        /// </summary>
        /// <param name="jsonData">JSON data</param>
        /// <param name="context">Database context</param>
        /// <returns>true if the message was stored correctly or false otherwise</returns>
        public bool SaveMessage(string jsonData, DatabaseContext context);

        /// <summary>
        /// Update the message into the database
        /// </summary>
        /// <param name="referenceQueue">referenceQueue message from queue</param>
        /// <param name="context">Database context</param>
        public Task UpdateMessage(ReferenceQueue referenceQueue, DatabaseContext context);
    }
}
