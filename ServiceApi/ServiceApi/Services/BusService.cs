﻿using MassTransit;
using MessageModel;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceApi.DBContext;
using ServiceApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceApi.Service
{
    public class BusService : IBusService
    {
        private readonly ILogger<BusService> _logger;
        private readonly IPublishEndpoint _publishEndpoint;

        public BusService(ILogger<BusService> logger, IPublishEndpoint publishEndpoint)
        {
            _logger = logger;
            _publishEndpoint = publishEndpoint;
        }

        public bool SaveMessage(string jsonData, DatabaseContext context)
        {
            try
            {
                if (string.IsNullOrEmpty(jsonData)) { throw new Exception("jsonData is null or empty"); }
                Message message = new Message() { MessageContent = jsonData.ToString(), CreateDateTime = DateTime.Now };
                context.Messages.Add(message);
                context.SaveChanges();
                _logger.LogInformation($"Saved message [{message.MessageId}]");

                // Parse JSON and sum all values
                List<Dictionary<string, string>> values = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(message.MessageContent);
                int finalValue = 0;
                foreach (Dictionary<string, string> value in values)
                {

                    finalValue += Convert.ToInt32(value["value"]);
                }

                MessageQueue messageQueue = new MessageQueue() { IdMessage = message.MessageId, Value = finalValue };
                SendMessageToQueue(messageQueue);
                return true;
            }
            catch (Exception exp)
            {
                _logger.LogError("Error when saving message into database", exp);
                return false;
            }
        }

       

        public async void SendMessageToQueue(MessageQueue messageQueue)
        {
            await _publishEndpoint.Publish<MessageQueue>(messageQueue);
        }

        public Task UpdateMessage(ReferenceQueue referenceQueue, DatabaseContext context)
        {
            try
            {
                if (null == referenceQueue) { throw new Exception("referenceQueue is null"); }
                Message message = context.Messages.FirstOrDefault(item => item.MessageId == referenceQueue.IdMessage);
                message.ReferenceId = referenceQueue.IdReference;
                message.ReferenceDateTime = referenceQueue.ReferenceDateTime;
                message.UpdateDateTime = DateTime.Now;
                context.Update(message);
                context.SaveChanges();
                _logger.LogInformation($"Updated message [{message.MessageId}]");
            }
            catch (Exception exp)
            {
                _logger.LogError("Error when updating message into database", exp);
            }
            return Task.CompletedTask;
        }
    }
}
