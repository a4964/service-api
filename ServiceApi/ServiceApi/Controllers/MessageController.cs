using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ServiceApi.DBContext;
using ServiceApi.Models;
using ServiceApi.Service;
using System.Collections.Generic;
using System.Linq;

namespace ServiceApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessageController : ControllerBase
    {
        private readonly ILogger<MessageController> _logger;
        private DatabaseContext _databaseContext;
        private readonly IBusService _busService;

        public MessageController(ILogger<MessageController> logger, DatabaseContext context, IBusService busService)
        {
            _logger = logger;
            _databaseContext = context;
            _busService = busService;
        }

        // API invoked by the frontend
        [EnableCors("AllowCorsPolicy")]
        [HttpPost]
        public IActionResult PostMessage([FromBody] dynamic value)
        {
            _logger.LogInformation($"Getting data from ServiceFrontend: [{value}]");
            var saved = _busService.SaveMessage(value.ToString(), _databaseContext);
            if (saved) 
            {
                return Ok();
            }
            else
            {
                return StatusCode(500);
            }
        }

        // API for retrieving all the messages saved into the Database
        [HttpGet]
        public IList<Message> Get()
        {
            return (_databaseContext.Messages.ToList());
        }
    }
}
