using MassTransit;
using MessageModel;
using ServiceApi.DBContext;
using ServiceApi.Service;
using System;
using System.Threading.Tasks;

namespace ServiceApi
{
    internal class ReferenceConsumer : IConsumer<ReferenceQueue>
    {
        private DatabaseContext _databaseContext;
        private readonly IBusService _busService;

        public ReferenceConsumer(DatabaseContext databaseContext, IBusService busService)
        {
            _databaseContext = databaseContext;
            _busService = busService;
        }
        public async Task Consume(ConsumeContext<ReferenceQueue> context)
        {
            await Console.Out.WriteLineAsync($"Getting data from ServiceSatellite: IdMessage [{context.Message.IdMessage}] IdReference [{context.Message.IdReference}] ReferenceDateTime [{context.Message.ReferenceDateTime}]");
            await _busService.UpdateMessage(context.Message, _databaseContext);
        }
    }
}
